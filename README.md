Role Name
=========

This role install a ProxmoxVE over a Debian 11 existing install. 

Inspiring of Proxmox documentation (https://pve.proxmox.com/wiki/Install_Proxmox_VE_on_Debian_Buster) updated for proxmox 7 over Debian Bullseye (11). 

Requirements
------------

No requirement is needed. 

Role Variables
--------------

2 network variable for hosts file needs to be set in defaults/main.yml

Dependencies
------------

No dependency is required

Example Playbook
----------------
```
- hosts: servers
  roles:
    - role: ansible-proxmoxve-install
      internet_ip: [x.x.x.x]
      fqdn: [hostFqdn]
```

License
-------

GNU GPLv3

Author Information
------------------

Passionate Sysadmin : https://yanux.info
